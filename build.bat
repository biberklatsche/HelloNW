@echo off
set PATH_TO_7ZIP=C:\"Program Files"\7-Zip\7z.exe
set PATH_ICUDTLDAT=lib\icudtl.dat
set PATH_NWPAK=lib\nw.pak
set PATH_NWEXE=lib\nw.exe
set APP_NAME=HelloWorld
if not exist %PATH_ICUDTLDAT% (
	goto nwFileNotExist
)
if not exist %PATH_NWPAK% (
	goto nwFileNotExist
)
if not exist %PATH_NWEXE% (
	goto nwFileNotExist
)
if not exist %PATH_TO_7ZIP% (
	goto 7zipNotExist
)
goto build

:nwFileNotExist
echo Es fehlen Dateien im lib-Verzeichnis. 
echo Dort muessen sich folgende Dateien befinden: 
echo   icudtl.dat
echo   nw.pak
echo   nw.exe
echo Diese koennen von http://nwjs.io/ heruntergeladen werden.
goto eof

:7zipNotExist
echo Kann 7-Zip nicht finden. Bearbeite einfach diese Datei und setze den Pfad deiner 7-Zip Installation in der Variable 'PATH_TO_7ZIP'. 7-Zip kann hier heruntergeladen werden http://www.7-zip.de/ .

:build
del /s /q target
%PATH_TO_7ZIP% a -tzip target/%APP_NAME%.nw .\src\*
copy %PATH_ICUDTLDAT% target
copy %PATH_NWPAK% target
copy /b %PATH_NWEXE%+target\%APP_NAME%.nw target\%APP_NAME%.exe
del /s /q target\%APP_NAME%.nw

:eof