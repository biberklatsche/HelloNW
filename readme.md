# HelloWorld fuer NW.js

## Unter Windwos bauen
[7-Zip](http://www.7-zip.de/) für Windows herunterladen und installieren.

[NW.js](http://nwjs.io) für Windows herunterladen. Die Zip entpacken und folgende Dateien in den *lib/* Ordner kopieren.

* *icudtl.dat*
* *nw.pak*
* *nw.exe*

Danach einfach *build.bat* ausführen. Im *target/* Ordner befindet sich anschliessend eine *HelloWorld.exe*.